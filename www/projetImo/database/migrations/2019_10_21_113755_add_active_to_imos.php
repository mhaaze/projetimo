<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActiveToImos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //rajour de la colonne "active" dans la table (sera utilisée pour le contrôle du mail)
        Schema::table('imos', function (Blueprint $table) {
            $table->integer('active');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('imos', function (Blueprint $table) {
            //
        });
    }
}
