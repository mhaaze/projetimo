<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imos', function (Blueprint $table) {
            $table->timestamps();
            $table->bigIncrements('id');
            $table->string('titre');
            $table->string('pays');
            $table->string('ville');
            $table->string('cp');
            $table->integer('nb_pieces');
            $table->string('prix');
            $table->text('descr');
            $table->text('contact');
            $table->string('photo' , 255);
            $table->rememberToken();
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imos');
    }
}
