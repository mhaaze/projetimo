<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Imo;
use Faker\Generator as Faker;

$factory->define(Imo::class, function (Faker $faker) {
    return [
        //
        "titre" => $faker->realText(20),
        "pays"  => $faker->state(),
        "ville" => $faker->city(),
        "cp"  => $faker->postcode(),
        "nb_pieces" => $faker->numberBetween(1,10),
        "prix"  => $faker->randomFloat(),
        "descr" => $faker->realText(255),
        "contact"  => $faker->address(),
        "photo" => $faker->imageUrl(),
        "email" => $faker->email(),
        "active" => 1,
        "devise" => "CHF",
        






    ];
});
