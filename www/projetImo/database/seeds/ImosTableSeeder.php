<?php

use Illuminate\Database\Seeder;

class ImosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Imo::class,15)->create();
        
    }
}
