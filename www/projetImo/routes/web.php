<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('test', function () {
    return view('layouts.app');
});



Route::get('test3', function () {
    return view('imo.create');
});

Route::get('create', function () {
    return view('imo.create');
});


Route::get('validateToken/{token}/{id}', 'validateMailController@validateEmail');

Route::get('imocreated/{id}', 'EmailPreviewController@imocreated')->where('id','[0-9]');

Route::get('mailValidation', function () {
    return view('imo.mailWarning');
});

//conection imos.php class to imos controller 

Route::get('/deleteOldImos', function(){
    DB::table('imos')->where('created_at', '<', 'DATE_SUB(curdate(), INTERVAL 3 MONTH)')->delete();
});

Route::get('/imos/search', 'ImosController@search');

Route::resource('imos', 'ImosController');



Route::get('detail', function () {
    return view('imo.detail');
});