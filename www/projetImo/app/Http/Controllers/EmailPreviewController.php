<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imo;

class EmailPreviewController extends Controller
{
    public function imoCreated($id){
        $imo = Imo::find($id);
        $token = str_random(255);
        return view ('emails.imocreated', ['imo'=>$imo,'token'=>$token]);
    }

}
