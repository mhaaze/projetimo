<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
Use App\Traits\UploaderTrait;
Use App\Traits\MailValidationTrait;
use App\Imo;
use App\Mail\creationImo;


class ImosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Récupérer toutes les annonces immobilières et les stocker dans une variable
      
        $imos = Imo::orderBy("id", "desc")->simplePaginate(10);

        return view('imo.index')->with('imos', $imos);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('imo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    Use UploaderTrait;
    Use MailValidationTrait;

    public function store(Request $request)
    {
        // Valider et enregistrer les données de la nouvelle annonce
        $request->validate([
            'titre' => 'required|max:200',
            'pays' => 'required|max:200',
            'cp' => 'required|integer',
            'nb_pieces' => 'required|integer',
            'prix' => 'required|integer',
            'descr' => 'required|max:255',
            'contact' => 'required|max:200',
            'photo' => 'nullable|image|mimes:jpeg,jpg,png,gif,svg',
            'email' => 'required|email',
            'devise' => 'required|max:10'
        ]);                       
            
            $imo = new Imo();
            $imo->titre = $request->input("titre");
            $imo->pays = $request->input("pays");
            $imo->ville = $request->input("ville");
            $imo->cp = $request->input("cp");
            $imo->nb_pieces = $request->input("nb_pieces");
            $imo->prix = $request->input("prix");
            $imo->descr = $request->input("descr");
            $imo->contact = $request->input("contact");
            $imo->email = $request->input("email");
            $imo->devise = $request->input("devise");

            // annonce iniactive à la creation
            $imo->active = 0;


            // Check if a profile image has been uploaded
        if ($request->has('photo')) {
            // Get image file
            $image = $request->file('photo');
            // Make a image name based on user name and current timestamp
            $name = str_slug($request->input('name')).'_'.time();
            // Define folder path
            $folder = '/uploads/images/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            $imo->photo = $filePath;
        }
                      
          $imo->save();

          //Envoi du mail de confirmation
          $this->sendEmail($imo->email, $imo->id);
          //Redirection vers la page d'accueil(index.php)

          return redirect('/mailValidation');
         

        
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $imo = Imo::findOrFail($id);

         return view('imo.detail')->with('imo', $imo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Récupérer la recette dans la base de données
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     /**
     * search box .
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    /*public function search(Request $request)
    {
        //name of the input should always be the name of the form store them in variable 
        //look at second commment after this to under
        $pays = $request->input("pays");
        $ville = $request->input("ville");
        $nb_pieces = $request->input("nb_pieces");
        
       
      //when you store them in variable use the variable here
        $imos = Imo::where("pays","like", "%$pays%")
                 ->orWhere("ville", "like", "%$ville%")
                 ->orWhere("nb_pieces","like", "%$nb_pieces%")->simplePaginate(10);
                 //->orwhere("prix","like" ,"%$search%")

       
        return view("imo.index")->with("imos", $imos);
    }*/
    
    public function search(Request $request)

    { 
        //the request which come from search form box 
        //will be asign to veriable 
        $pays = $request->input("pays", "");
        $ville = $request->input("ville", "");
        $nb_pieces = 0;
        if($request->filled("nb_pieces")){
            $nb_pieces = $request->input("nb_pieces");
        }
      

        $imos = Imo::where("pays","like", "%$pays%")
                 ->where("ville", "like", "%$ville%")
                 ->where("nb_pieces",">=", $nb_pieces)->simplePaginate(10);

        return view('imo.index', ["imos" => $imos,
                                "pays" => $pays,
                                "ville" => $ville,
                                "nb_pieces" => $nb_pieces]);

    }
}
