<?php

namespace App\Http\Controllers;

//use App\Http\Request;
use App\mailValidation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Imo;

class validateMailController extends Controller
{
    public function show()
    {
        return view ('imo.mailValidation');
    }

    public function validateEmail ($token,$id){
        // recuperer l'annonce par son id
        $imo=Imo::find($id);

        // Verifier le token et passer l'annonce à active
        if ($token ==  $imo->remember_token){
            $imo->active=1;
            $imo->save();
        }


        // retourner la vue de l'annonce
        return view('imo.detail')->with('imo', $imo);
    }

    /*public function sendToken(Request $request, mailValidation $auth)
    {
        $auth->requestLink();

        $request->validate(['email' => 'required|email|max:255']);
    }*/
    
   
}
