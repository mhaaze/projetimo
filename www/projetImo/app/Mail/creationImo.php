<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Imo;

class creationImo extends Mailable
{
    use Queueable, SerializesModels;

    private $imo;
    private $token;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Imo $imo, $token)
    {
        //atribut imo avec l'imo passé en paramètre
        $this->imo = $imo;
        $this->token =$token;
    }



    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view ('emails.imocreated', ['imo'=>$this->imo,'token'=>$this->token]);
    }
}
