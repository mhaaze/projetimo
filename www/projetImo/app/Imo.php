<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imo extends Model
{
    //
    protected $fillable = [
        'titre',
        'pays',
        'cp',
        'nb_pieces',
        'prix',
        'devise',
        'descr',
        'contact',
        'email',
        
    ];

}
