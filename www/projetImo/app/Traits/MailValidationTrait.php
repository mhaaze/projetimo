<?php

namespace App\Traits;

use Illuminate\Support\Facades\Mail;
use App\Mail\creationImo;
use App\Imo;

trait MailValidationTrait
{
    public function sendEmail($email, $id){

        $imo=Imo::find($id);
        $token = str_random(25);
        $imo->remember_token=$token;
        $imo->save();
        Mail::to($email)->send(new creationImo($imo,$token));

    }

   
}