<html>
    <head>
        
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
        
            <!-- CSRF Token -->
            <meta name="csrf-token" content="{{ csrf_token() }}">
        
            <title>{{ config('app.name', 'Laravel') }}</title>
        
            <!-- Scripts -->
            <script src="{{ asset('js/app.js') }}" defer></script>
        
            <!-- Fonts -->
            <link rel="dns-prefetch" href="//fonts.gstatic.com">
        
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
            
        
            <!-- Styles -->
            <link href="{{ asset('css/app.css') }}" rel="stylesheet">
            <link href="{{ asset('css/main.css')}}" rel="stylesheet">
        </head>
   
    <body>
        <div class="py-4">
            <div class="container-flex ">
                <nav class="navbar navbar-primary nav2 d-flex bd-highlig ">
                    <a href="{{asset('imos')}}"> <img class="image" src="{{ asset('images/home.png') }}"> </a>
                    <a class="navbar-brand p-2 flex-grow-1 bd-highlight">Site d'annonces immobliere</a>
                    <a href="{{asset('create')}}" class="btn btn-outline-secondary  bn but btn2">Ajouter une annonce</a>               
               </nav>
            </div>
            <main>
                @yield('content')
            </main>
    </div>
    </body>
</html>