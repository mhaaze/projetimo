@extends('layouts.app')

@section("content")


<div class="needs-validation container taille">

    @if($errors->any())
        {{ $errors }}
        <div class="alert alert-danger">
            <ul>
                @foreach($errors as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    <form action="/imos" method="POST" enctype="multipart/form-data" >
        @csrf 
        <div class="Ajout form-row">
            <div class="col-4">
                <label for="ajouterAnnonce"></label>
                <input type="text" name="titre" class="form-control @error('titre') is-invalid @enderror " value= "{{ old('titre')}}" required placeholder="Titre de l'annonce">
            </div>
            <div class="UploadPhoto col-4">
                    <div class="form-group downI">
                        <label for="exampleFormControlFile1">Image <input type="file" name="photo" class="form-control-file @error('image') is-invalid @enderror" id="imageFormControlFile1" value= "{{ old('image') }}" required></label>
                        
                    </div>
                </div>
            <div class="Coordonnees col-4">
                <div class="form-group">
                    <label for="vousContacter"></label>
                    <textarea name="contact" class="form-control @error('') iscontact-invalid @enderror" rows="6" value="{{ old('contact')}}" required placeholder="Vos coordonnées"></textarea>
                </div>                        
            </div>
        </div>      
        
    

        <div class="Descr container-fluid">
            <div class="form-row">
                <div class="col-12">
                    <textarea name="descr"  value= "{{ old('descr') }}" class="form-control @error('descr') is-invalid @enderror" rows="7" placeholder="Description" required></textarea><br>
                </div> 
            </div>   

            <div class="container-fluid">
                <div class="form-row">
                    <div class="col-4">           
                        <!--<label for="Pays">Pays</label>-->
                        <input type="text" placeholder="Pays" name="pays" value= "{{ old('pays') }}" class="form-control @error('pays') is-invalid @enderror">                   
                    </div> 
                    
                    <div class="col-4">
                        <!--<label for="ville">Ville</label>-->
                        <input type="text" name="ville" placeholder="Ville" value= "{{ old('ville') }}"class="form-control @error('ville') is-invalid @enderror" id="Ville ">        
                    </div>

                    <div class="col-2 sm-1">
                        <!--<label for="case_postale">Case postale</label>-->
                        <input type="value" name="cp" placeholder="Case Postale" value= "{{ old('cp') }}" class="form-control @error('ville') is-invalid @enderror" id="inputZip">
                    </div>
                </div> 
            </div><br>                                                
            
            <div class="container-fluid">
                <div class="form-row">
                    <div class="col-3 -sm-2">          
                        <select class="browser-default custom-select" name="nb_pieces" value= "{{ old('nb_pieces') }}">
                        <option text="offre">Nombre de pieces</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="6">6 et +</option>
                        </select>
                    </div> 

                    <div class="col-2 sm-1">                                           
                        <input type="value" class="form-control @error('prix') is-invalid @enderror" name="prix" value= "{{ old('prix') }}" placeholder="Prix">      
                    </div> 
                    
                    <div class="col-2 mr-3 sm-2 ">
                       <!-- <label for="devise"></label>  -->
                        <select class="browser-default custom-select" name="devise" value= "{{ old('devise') }}">
                            <option value="CHF">CHF</option>
                            <option value="€">€</option>
                        </select>
                    </div>
                    <div class="row col-3 mr-2 sm-2">
                       <!-- <label for="myEmail">Email</label> -->
                        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="myEmail" placeholder=" Enregistrez votre email" required>          
                    </div>
                </div>
            </div><br>               
                                        
            
            <div class="conditionAjout container-fluid">                               
                <div class="conditionApprouv row ml-1 col-12 sm-4">
                    <p><B>Conditions générales:</B><br>                            
                    Le Lorem Ipsum est simplement du faux texte employé dans la composition et 
                    la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un 
                    imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait 
                    que survivre cinq siècles, mais s'est aussi adapté à la                                      
                    </p>                                        
                </div>

                <div class="conditionApprouv row col-12 sm-4 condi">           
                    <input class="form-check-input  @error('checkbox') is-invalid @enderror" type="checkbox" value="" id="invalidCheck266" required>                                                                                
                    <p> J'ai bien lu les conditions et je les ai comprise</h6>
                    <div class="ajouter"> 
                    <button class="btn btn-primary btn-sm btn-rounded bCondi" type="submit">Ajoutez votre annonce</button>
                    </div>                                           
                </div>
            </div>
        </div>           
    </form>             
</div>     

@endsection      
         
                        


                

                   
