@extends("layouts.app")
@section("content")



<div class="container d-flex flex-wrap full-heigh detailCadre">
    <div class="row ">
        <div class="col-sm bg-light border border-dark">
            <div class="row detail">
                <div class="col-sm">
                    <img class="imgd img-fluid" src="{{$imo->photo}}"> 
                </div>
                <div class="col-3"> 
                    <h4>{{$imo->titre}} </h4>
                    <h4>Pays: {{$imo->pays}} </h4>
                    <h4>Ville: {{$imo->ville}} </h4>
                    <h4>{{$imo->nb_pieces}} pièces</h4>
                </div>
                <div class="col-3 contacteA">
                    <b>Comment contacter l'annonceur:</b><br>
                    {{$imo->contact}}
                </div>                              
            </div>
            
            <div class="row p-2 prixD">                               
                <b> {{$imo->prix}} {{$imo->devise}} TCC </b>
            </div>
            <div>                        
                <div class="row p-2"> <p class="descri">{{$imo->descr}}</p></div>
            </div>
            
        </div>  
    </div>      



   
</div>
</div>



@endsection