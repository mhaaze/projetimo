@extends('layouts.app')

@section('content')
<div class="container taille">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form method="POST" >
                        @csrf

                        <div class="form-group row">
                            

                            <div class="col-md-6">
                                
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12  text-center">
                                <h3>Validation de votre annonce</h3>
                                <h5> Veuillez suivre les instructions de l'email qui vous a été envoyé</h5>


                            <a href="{{ url('/imos') }}" class="btn btn-primary"> Retour à l'index</a>
                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection