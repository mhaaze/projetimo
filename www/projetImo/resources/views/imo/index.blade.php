@extends("layouts.app")

@section("content")
<div class="container-fluid pb-4 filtre filtreI">

     
        <form action="/imos/search" method="GET" class="form-row">
            @csrf

                <b class="fp"> Filter Par : </b>
                    <div class="col-sm">
                    <label for="pays"></label>
                    <input type="text" class="form-control custom-select" name="pays" placeholder="pays" value="@if(isset($pays)){{$pays}}@endif" >
                    </div>
                    <div class="col-sm">
                    <label for="Ville"></label>
                    <input type="text" class="form-control custom-select" name="ville" placeholder="ville" value="@if(isset($ville)){{$ville}}@endif">
                    </div>
                    <div class="col-sm">
                    <label for="nb"></label>
                    <input type="number" min="1" max="6" class="form-control custom-select" name="nb_pieces" placeholder="Nombre de pieces" value="@if(isset($nb_pieces)){{$nb_pieces}}@endif">
                    </div>
                    <br>
                    <button type="submit" class="btn btn-secondary but">Rechercher </button>
                   
        </form>
   

                <div class=" row pt-3 ml-5">
                
                    <div class="col-sm-3 align-self-end offset-9 ">
                        <b>  Trier par</b> 
                            <select class="browser-default custom-select2">                                            
                                <option value="asc">Prix croissant</option>
                                <option value="desc">Prix décroissant</option>
                            </select>                                        
                    </div>
                </div>

    </div>

    <div class="container">
        @if($imos->count() > 0)
            @foreach($imos as $imo)
                <div class="list-group pb-1">
                   @if($imo->active ==1) 
                        <a class="list-group-item list-group-item-warning"  href="{{URL::to('imos/'.$imo->id)}}">
                            <div class="row">
                                <div class="col">                                
                                    <img class="imgs" src="{{$imo->photo}}">
                                </div>
                                
                                <div class="col">                                    
                                    <h4>{{$imo->titre}}  </h4>
                                    <h4>{{$imo->ville}}  </h4>
                                <h4> {{$imo->nb_pieces}} pieces <h4>

                                </div>
                                    <div class="col offset-5">
                                    <b> {{$imo->prix}} </b>
                                    <b> {{$imo->devise}} TCC </b>
                                </div>
                            </div>
                        </a>
                    @endif               
                </div>
                @endforeach
                    @if(isset($pays) || isset($ville) || isset($nb_pieces))
                        <div class="d-flex justify-content-center">
                          {{$imos->appends(["pays" => $pays, "ville" => $ville, "nb_pieces" => $nb_pieces])->links() }}
                        </div>
                    @else
                        <div class="d-flex justify-content-center">
                            {{$imos->links() }}
                          </div>
                    @endif
        @else
            <div class="row r2">
                <div class="col text-center alert alert-secondary msg">
                    <h1>Il n'y a pas d'annonces correspondant à votre recherche</h1>
                </div>
            </div>
                
        @endif

    </div>
            
    </div>
    
   

@endsection