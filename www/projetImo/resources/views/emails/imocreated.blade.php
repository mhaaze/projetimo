<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, inital-scale=1" name="viewport">
    <title></title>

    <link role="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

</head>

<body class="text-center">
    <div class="container">
        <div class="card">
                <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3>Cher membre,</h3>
                    <p>Vous venez de créer une nouvelle annonce :<span class="font-weight-bold">{{ $imo->titre }}</span><br></p>

                    <p>Pour la valider,  veuillez cliquer sur le lien suivant: <br></p>
                </div>
            </div>
                </div>
        <div class="card-footer">
            <div class="row d-flex justify-content-center">
                <a class="btn btn-primary" href="{{ URL::to("validateToken/$token/$imo->id") }}">
                    Validez votre article</a>
            </div>
        </div>
        </div>
    </div>                                                

</body>
</html>